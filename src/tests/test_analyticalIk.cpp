/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <youbot_parser/YouBotAnalyticalSolver.hpp>
#include <youbot_parser/KDL_ModelParser_inline.hpp>
#include <stdio.h>


int main(int argc, char **argv) {

	KDL::JntArray jointpositions(5);

	KDLModelParser test;
	KDL::Frame EndeffectorFrame;

	KDL::JntArray testpostions(5);

	if(test.forwardKinematics())
	{
		EndeffectorFrame = test.cartpos;
	}
	YouBotAnalyticalSolver ybIKobj;
	bool x = ybIKobj.ComputeIK(EndeffectorFrame);
	Eigen::MatrixXf SolutionsInLimit;
	SolutionsInLimit.resize(4,8);

	KDL::JntArray LowerLimits;
	KDL::JntArray UpperLimits;
	LowerLimits = KDL::JntArray(5);
	UpperLimits = KDL::JntArray(5);
	LowerLimits(0) = -0.1;
	LowerLimits(1) = -0.1;
	LowerLimits(2) = -0.1;
	LowerLimits(3) = -0.1;
	LowerLimits(4) = -0.1;
	UpperLimits(0) = 0.1;
	UpperLimits(1) = 0.1;
	UpperLimits(2) = 0.1;
	UpperLimits(3) = 0.1;
	UpperLimits(4) = 0.1;

	if(x == 1)
	{
		std::cout<<" IKSolutions "<< std::endl<<ybIKobj.IKSolutions <<std::endl;
		for(int i = 0; i <= 3; i++)
		{
			int k = 0;
			for (int j = 1; j <= 5; j++)
			{
				if(ybIKobj.IKSolutions(i,j) < UpperLimits(k) && ybIKobj.IKSolutions(i,j) > LowerLimits(k))
				{
					SolutionsInLimit(i,j) = ybIKobj.IKSolutions(i,j);
					k++;
				}
				else
				{
					SolutionsInLimit(i,j) = -256;
				}
			}
		}
	}
	std::cout << "SolutionsInLimit" << std::endl <<SolutionsInLimit << std::endl;

	return 0;
}









