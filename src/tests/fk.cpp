<<<<<<< HEAD

#include "kinematics_msgs/GetPositionIK.h"
#include "std_msgs/Float32MultiArray.h"
#include <kdl_parser/kdl_parser.hpp>
//#include <tf_conversions/tf_kdl.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <kdl/chainfksolverpos_recursive.hpp>
//#include <cob_mmcontroller/augmented_solver.h>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/jntarray.hpp>
#include <youbot_parser/SensorBaseClass.h>
#include <youbot_parser/ActuatorBaseClass.h>
=======
/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <youbot_parser/KDL_ModelParser_inline.hpp>
#include <iostream>
#include <youbot_parser/message_conversions_inline.hpp>
#include <Eigen/src/SVD/JacobiSVD.h>
#include <Eigen/src/Jacobi/Jacobi.h>
#include <Eigen/SVD>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <math.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
using namespace Eigen;

KDL::JntArray armpositions(5);
std_msgs::Float32MultiArray jacobian;
double tolerance=0.001;

bool checkfortolerance(Eigen::VectorXd &sensor)
{
	double tmp;
	for (unsigned int i=0;i<6;i++){
		if (sensor(i) < 0){
			tmp = -sensor(i);
		}
		else{
			tmp = sensor(i);
		}
		if (tmp < tolerance)
			sensor(i) = 0.0;
	}
	std::cout << sensor << std::endl;
	if (sensor(0)==0 && sensor(1) ==0 && sensor(2) == 0 && sensor(3) == 0 && sensor(4) == 0 && sensor(5) == 0){
		return true;
	}

	return false;
}

void setValues(const float values[])
{
	armpositions(0) =   values[0];
	armpositions(1) =    values[1];
	armpositions(2) =  values[2];
	armpositions(3) =  values[3];
	armpositions(4) =  values[4];

}

int main(int argc, char **argv) {

	KDLModelParser *kdl;
	HardwareEncapsulation::SensorBaseClass *sensed;
	HardwareEncapsulation::ActuatorBaseClass *actuate;
	KDL::Chain chain;
	KDL::JntArray armsensed(5);
	float x =-1.57,y =-1.57,z=-1.57,q=-1.57,w=-1.57;
	KDL::Jacobian jac(5);
	KDL::Jacobian jac_t(5);
	KDL::Twist twist;
	KDL::Frame cartpos1;
	KDL::Frame cartpos2;
	Eigen::Matrix<double,5,6> _jac;
	Eigen::Matrix<double,6,6> prod;
	sensed = new HardwareEncapsulation::SensorBaseClass();
	actuate = new HardwareEncapsulation::ActuatorBaseClass();
	kdl = new KDLModelParser();
	chain = kdl->chain;
	KDL::ChainJntToJacSolver *jacsolver;
	Eigen::VectorXd sensor(6);
	Eigen::VectorXd jntsolns(5);
	jacsolver = new KDL::ChainJntToJacSolver(chain);
        boost::thread_group tgroup;
	float values[] = {2.96244,2.3883,-1.57,2.5,3};
	setValues(values);
	actuate->ActuateArm(armpositions);
	sensed->SensorUpdate();
	armsensed = sensed->JointAngles;
	x = armsensed(0);
	y = armsensed(1);
	z = armsensed(2) ;
	q = armsensed(3);
	w = armsensed(4);
	kdl->forwardKinematics(armsensed,cartpos1);
	sensor << atof(argv[1]), atof(argv[2]),atof(argv[3]),atof(argv[4]),atof(argv[5]),atof(argv[6]);

	while (true)
	{
		{
			jacsolver->JntToJac(armsensed,jac);
			MatrixXd m = jac.data;
			JacobiSVD<MatrixXd> svd(m, ComputeThinU | ComputeThinV);
			jntsolns = svd.solve(sensor);

		}
		x=x+jntsolns(0);
		y=y+jntsolns(1);
		z=z+jntsolns(2);
		q=q+jntsolns(3);
		w=w+jntsolns(4);

		values = {x,y,z,q,w};
		setValues(values);
		tgroup.create_thread(boost::bind(&HardwareEncapsulation::ActuatorBaseClass::ActuateArmRealtime,actuate,armpositions));
		//actuate->ActuateArmRealtime(armpositions);
		sensed->SensorUpdate();
		armsensed = sensed->JointAngles;

		kdl->forwardKinematics(armsensed,cartpos2);


		double diffx = (cartpos2.p.x()-cartpos1.p.x());
		double diffy = (cartpos2.p.y()-cartpos1.p.y());
		double diffz = (cartpos2.p.z()-cartpos1.p.z());
		sensor <<sensor(0)-diffx,sensor(1)-diffy,sensor(2)-diffz,sensor(3),sensor(4),sensor(5);
		if (!checkfortolerance(sensor)){
			jacsolver->JntToJac(armsensed,jac);
			sensed->SensorUpdate();
			armsensed = sensed->JointAngles;

			//kdl->forwardKinematics(armsensed,cartpos1);
			SLEEP_MILLISEC(32);
			tgroup.join_all();
		}
		else
			break;



	}










	return 0;
}


