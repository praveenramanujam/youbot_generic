
#include "ikfast.cpp"
#include <openrave-0.9/openrave-core.h>
#include <vector>
#include <cstring>
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/format.hpp>

using namespace OpenRAVE;
int main(int argc, char** argv)
{


	RaveInitialize(true); // start openrave core
	EnvironmentBasePtr penv = RaveCreateEnvironment(); // create the main environment
	{
		// lock the environment to prevent changes
		EnvironmentMutex::scoped_lock lock(penv->GetMutex());
		//Transform trans;
		TransformMatrix t;
		unsigned int i = 0;
		while(1){
		t = matrixFromAxisAngle (Vector(RaveRandomFloat()-0.5,RaveRandomFloat()-0.5,RaveRandomFloat()-0.5));
		t.trans = Vector(RaveRandomFloat()-0.5,RaveRandomFloat()-0.5,RaveRandomFloat()-0.5)*2;
		std::vector<IKSolution> vsolutions;
		std::vector<IKReal> vfree(getNumFreeParameters());
		IKReal eerot[9],eetrans[3];
		eerot[0] = t.rot(0,0); eerot[1] = t.rot(0,1); eerot[2] = t.rot(0,2); eetrans[0] = (RaveRandomFloat()-0.5)*2;
		eerot[3] =  t.rot(1,0); eerot[4] =  t.rot(1,1); eerot[5] =  t.rot(1,2); eetrans[1] =(RaveRandomFloat()-0.5)*2;
		eerot[6] =  t.rot(2,0); eerot[7] =  t.rot(2,1); eerot[8] =  t.rot(2,2); eetrans[2] = (RaveRandomFloat()-0.5)*2;
		for(std::size_t i = 0; i < vfree.size(); ++i)
			vfree[i] = atof(argv[13+i]);
		bool bSuccess = ik(eetrans, eerot, vfree.size() > 0 ? &vfree[0] : NULL, vsolutions);

		if( !bSuccess ) {
			//fprintf(stderr,"Failed to get ik solution\n");
			//return -1;
		}

		//printf("Found %d ik solutions:\n", (int)vsolutions.size());
		std::vector<IKReal> sol(getNumJoints());
		std::vector<IKReal> jointPositions(getNumJoints());
		for(std::size_t i = 0; i < vsolutions.size(); ++i) {
			printf("sol%d (free=%d): ", (int)i, (int)vsolutions[i].GetFree().size());
			std::vector<IKReal> vsolfree(vsolutions[i].GetFree().size());
			vsolutions[i].GetSolution(&sol[0],vsolfree.size()>0?&vsolfree[0]:NULL);
			for( std::size_t j = 0; j < sol.size(); ++j)
			{
				jointPositions[j] = sol[j];
				//jointPositions.assign (j,sol[j]);
				//jointPositions.assign = sol[j];
				//printf("%.15f, ", sol[j]);
			}

			//printf("%.15f, ", jointPositions[2]);
			for (int j =0; j<=4;j++)
			std::cout<<jointPositions[j]<< std::endl;
			printf("\n");
		}
	        if( bSuccess ) {
			//fprintf(stderr,"Failed to get ik solution\n");
			i = i+1;
		}
	        if(i >3)
	        	return 0;
	}
	}
	return 0;
}
