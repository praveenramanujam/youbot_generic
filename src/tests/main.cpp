#include <youbot_parser/velocity_iterator.h>
#include <stdio.h>
#include <iostream>

int main()
{
	for(PredictiveController::VelocityIterator x_it(-2.0, 2.0, 0.02); !x_it.isFinished(); x_it++){
	      double vel_sample = x_it.getVelocity();
 	      std::cout << vel_sample << std::endl;
	}
}
