/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/




#include <youbot_parser/ActuatorBaseClass.h>
#include <youbot_parser/SensorBaseClass.h>

#include <iostream>


KDL::JntArray armpostions(5);

void setValues(const float values[])
{
	armpostions(0) =   values[0]; //0.0100692 rad and 5.84014 rad
	armpostions(1) =    values[1]; //(0.0100692 rad and 2.61799 rad)
	armpostions(2) =  values[2]; //-5.02655 rad and -0.015708 rad
	armpostions(3) =  values[3];  //0.0221239 rad and 3.4292
	armpostions(4) =  values[4];//0.110619 rad and 5.64159 rad

}

int main(int argc, char **argv) {

	try{

		HardwareEncapsulation::ActuatorBaseClass *actuator;
		HardwareEncapsulation::SensorBaseClass *sensor;
		KDL::JntArray jnt(5);

		actuator = new HardwareEncapsulation::ActuatorBaseClass();
		sensor = new HardwareEncapsulation::SensorBaseClass();


		sensor->SensorUpdate();
		jnt = sensor->JointAngles;
		std::cout << jnt(0) << "," << jnt(1) << "," << jnt(2) << "," << jnt(3) << "," << jnt(4)<<  std::endl;
		SLEEP_SEC(5);

		float values[] = {0.34035644,0.06369958,1.45498277,1.1113838,0.37794839};
		setValues(values);
		actuator->ActuateArm(armpostions);

		/*
		values = {1.1741209,0.00307822,0.28675643,-1.43863347,-1.70116861};
		setValues(values);
		actuator->ActuateArm(testpostions);


		values = {-1.96747176,-0.06742393,0.34800588,0.86821687,1.44042405};;
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= {-1.96747176,-0.15676719,0.57358027,0.73198574,1.70116861};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= {-1.96747176,0.25633754,-0.34800588,1.24046716,1.44042405};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { -1.96747176,0.37614001,-0.57358027 ,1.34623908 ,1.70116861};
		setValues(values);
		actuator->ActuateArm(testpostions);




		float values[] = {2.9,0.0,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);


		values = {-2.9,0.0,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);


		values = {0.0,1.4,0.0,0.0,0.0};;
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,-1.0 ,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0,2.5,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);



		values= { 0.0,0.0 ,-2.6,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,1.6,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,-1.6,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,0.0};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,2.9};
		setValues(values);
		actuator->ActuateArm(testpostions);

		values= { 0.0,0.0 ,0.0,0.0,-2.9};
		setValues(values);
		actuator->ActuateArm(testpostions);*/










		//std:cout << cartpos << std::endl;

	}
	catch (std::exception& e) {

		std::cout <<"Exception: "<< e.what() << std::endl;

	}
	catch (...) {

		std::cout << "unhandled exception" << std::endl;
	}


	return 0;
}







