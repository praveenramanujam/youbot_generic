/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#include <youbot_parser/YouBotAnalyticalSolver.hpp>

/*Step 1: Calculation of beta and theta given 6 dimensional goal pose
Step 2: Calculate rotation of the platform theta. row1 = 0
Step 3: Given row2 and row3, calculate theta2, theta3 and theta4
Step 4: Calculate x and y positions.
Step 5: Correct theta, and given row1, calculate theta1.*/

void YouBotAnalyticalSolver::getGoalPoseFromUser()
{
	double temp;

	for (int i = 0; i < 6; i++)
	{
		std::cout << "Enter 6D goal position" << i+1
				<< ": " << std::flush;
		std::cin >> temp ;
		sixdGoalPose.push_back(temp);
	}
	//for (int i = 0; i < 6; i++)
		//std::cout << sixdGoalPose.at(i) << std::endl;
}

void YouBotAnalyticalSolver::convertFKPoseToEuler(KDL::Frame &FKPose,double &alphaFromFK,double &betaFromFK,double &gamaFromFK)
{
	//FKPose.M.
	//std::cout<<"Alpha before           " <<alphaFromFK <<"Beta            "<< betaFromFK <<"Gama            "<< gamaFromFK<< std::endl;
	FKPose.M.GetEulerZYX(alphaFromFK, betaFromFK, gamaFromFK);
	//std::cout<<"Alpha inside           " <<alphaFromFK <<"Beta            "<< betaFromFK <<"Gama            "<< gamaFromFK<< std::endl;
	//FKRotInEuler.GetEulerZYX(alphaFromFK, betaFromFK, gamaFromFK);
	//std::cout<<"FKROTINEULER" << FKRotInEuler << std::endl;
	FKVector = FKPose.p;
	//std::cout << "FKRotInEuler "<<FKRotInEuler <<std::endl;
	//std::cout<< "FKVector "<<FKVector<<std::endl;
}

void YouBotAnalyticalSolver::setUserInputAsFKinput(double &alphaFromFK,double &betaFromFK,double &gamaFromFK)
{
	sixdGoalPose.at(0) = FKVector.x();
	sixdGoalPose.at(1) = FKVector.y();
	sixdGoalPose.at(2) = FKVector.z();
	sixdGoalPose.at(3) = alphaFromFK;
	sixdGoalPose.at(4) = betaFromFK;
	sixdGoalPose.at(5) = gamaFromFK;
	//for (int i = 0; i < 6; i++)
		//std::cout << sixdGoalPose.at(i) << std::endl;
}

void YouBotAnalyticalSolver::calculateBetaAndTheta(double &Beta)
{
	double goalRoll = sixdGoalPose.at(5);  //Gama
	double goalPitch = sixdGoalPose.at(4); //Beta
	double goalYaw = sixdGoalPose.at(3);   //Alpha

	KDL::Rotation rotMat = KDL::Rotation::EulerZYX(goalYaw,goalPitch,goalRoll);
	double r31 = rotMat(0,2);
	double r32 = rotMat(1,2);
	double r33 = rotMat(2,2);
	//std::cout<<"RotationMatrix: "<<rotMat<< std::endl;
	//std:: cout << r31 << std::endl << r31 << std::endl << r33 << std::endl;
	Beta = atan2(r33,sqrt((r31*r31)+(r32*r32)));
	//std::cout<<"Beta: " << Beta<< std::endl;
	double Theta = atan2(r32,r31);
	this->IKSolutions(0,0) = Theta;
	this->IKSolutions(1,0) = Theta;
	this->IKSolutions(2,0) = Theta;
	this->IKSolutions(3,0) = Theta;

	this->IKSolutions(0,1) = Theta;
	this->IKSolutions(1,1) = Theta;
	this->IKSolutions(2,1) = Theta;
	this->IKSolutions(3,1) = Theta;

	//std:: cout <<"The value of Beta is "<< Beta << std::endl;
	//std:: cout <<"The value of Theta is "<< Theta << std::endl;
}

void YouBotAnalyticalSolver::calculateThetaThree(const double &Beta,double &Theta3Positive,double &Theta3Negative)
{
	double xDash = paramObj.rhoTwo;        // rho2 can vary within a certain range
	double zDash = paramObj.verticalVariance;

	double zDoubleDash = zDash - (LengthSegment[3] * sin(Beta));
	//std::cout << "zDoubleDash: " << zDoubleDash << std::endl;
	double xDoubleDash = xDash - (LengthSegment[3] * cos(Beta));
	//std::cout << "xDoubleDash: " << xDoubleDash << std::endl;
	double numerator1 = -(zDoubleDash * zDoubleDash) - (xDoubleDash * xDoubleDash) + (LengthSegment[1] * LengthSegment[1]) + (LengthSegment[2] * LengthSegment[2]);
	//std::cout << "numerator1: " << numerator1 << std::endl;
	double denominator1 = 2 * LengthSegment[1] * LengthSegment[2];
	//std::cout << "denominator1: " << denominator1 << std::endl;
	double cosTheta3 = (numerator1/denominator1);
	//std::cout << "cosTheta3: " << cosTheta3 << std::endl;
	double sinTheta3Positive = sqrt(1 - (cosTheta3 * cosTheta3)); //There are two values for sine
	//std::cout << "sinTheta3Positive: " << sinTheta3Positive << std::endl;
	double sinTheta3Negative = -(sqrt(1 - (cosTheta3 * cosTheta3))); //One positive and one negative
	//std::cout << "sinTheta3Negative: " << sinTheta3Negative << std::endl;

	Theta3Positive = atan2(sinTheta3Positive,cosTheta3);

	//std::cout << "The value of thetaThree+ is " << Theta3Positive << std::endl;
	Theta3Negative = atan2(sinTheta3Negative,cosTheta3);
	//std::cout << "The value of thetaThree- is " << Theta3Negative << std::endl;

	this->IKSolutions(0,3) = Theta3Positive;
	this->IKSolutions(1,3) = Theta3Negative;
	this->IKSolutions(2,3) = Theta3Positive;
	this->IKSolutions(3,3) = Theta3Negative;
}

void YouBotAnalyticalSolver::calculateThetaTwo(const double &Beta,double &Theta2Positive,double &Theta2Negative,const double &Theta3Positive,const double &Theta3Negative)
{
	double xDash = paramObj.rhoTwo;        // rho2 can vary within a certain range
	double zDash = paramObj.verticalVariance;

	// For positive value of thetaThree
	double k1 = LengthSegment[1] - (LengthSegment[2] * cos(Theta3Positive));
	double k2 = LengthSegment[2] * sin(Theta3Positive);

	double gamaPositive = atan2(k2,k1);
	double zDoubleDash = zDash - (LengthSegment[3] * sin(Beta));
	double xDoubleDash = xDash - (LengthSegment[3] * cos(Beta));
	Theta2Positive = atan2(zDoubleDash,xDoubleDash) + gamaPositive;
	//std::cout << "The value of thetaTwo+ is " << Theta2Positive << std::endl;

	// For negative value of thetaTwo
	double k3 = LengthSegment[1] - (LengthSegment[2] * cos(Theta3Negative));
	double k4 = LengthSegment[2] * sin(Theta3Negative);

	double gamaNegative = atan2(k4,k3);
	Theta2Negative = atan2(zDoubleDash,xDoubleDash) + gamaNegative;
	//std::cout << "The value of thetaTwo- is " << Theta2Negative << std::endl;

	this->IKSolutions(0,2) = Theta2Positive;
	this->IKSolutions(1,2) = Theta2Positive;
	this->IKSolutions(2,2) = Theta2Negative;
	this->IKSolutions(3,2) = Theta2Negative;
}

void YouBotAnalyticalSolver::calculateThetaFour(const double &Beta,const double &Theta2Positive, const double &Theta2Negative,const double &Theta3Positive,const double &Theta3Negative)
{
	double Theta4Positive = Theta2Positive + Theta3Positive - Beta;
	double Theta4Negative = Theta2Negative + Theta3Negative - Beta;
	double Theta4PosNeg = Theta2Positive + Theta3Negative - Beta;
	double Theta4NegPos = Theta2Negative + Theta3Positive - Beta;
	//std::cout << "The value of thetaFour+ is " << Theta4Positive << std::endl;
	//std::cout << "The value of thetaFour- is " << Theta4Negative << std::endl;
	//std::cout << "The value of thetaFour+- is " << Theta4PosNeg << std::endl;
	//std::cout << "The value of thetaFour-+ is " << Theta4NegPos << std::endl;

	this->IKSolutions(0,4) = Theta4Positive;
	this->IKSolutions(1,4) = Theta4PosNeg;
	this->IKSolutions(2,4) = Theta4NegPos;
	this->IKSolutions(3,4) = Theta4Negative;
}

void YouBotAnalyticalSolver::calculateBasePositionsXandY()
{
	double goalX = sixdGoalPose.at(1);
	double goalY = sixdGoalPose.at(2);

	double endEffectorX = FKVector.x();
	double endEffectorY = FKVector.y();

	double DeltaX = goalX - endEffectorX;
	double DeltaY = goalY - endEffectorY;
	//std::cout<<"The robot center should move to ("<< DeltaX<<", "<< DeltaY<<") coordinates."<<std::endl;

	this->IKSolutions(0,6) = DeltaX;
	this->IKSolutions(1,6) = DeltaX;
	this->IKSolutions(2,6) = DeltaX;
	this->IKSolutions(3,6) = DeltaX;

	this->IKSolutions(0,7) = DeltaY;
	this->IKSolutions(1,7) = DeltaY;
	this->IKSolutions(2,7) = DeltaY;
	this->IKSolutions(3,7) = DeltaY;
}

void YouBotAnalyticalSolver::calculateThetaFive(KDL::Frame &FKPose, double &alphaFromFK,double &betaFromFK,double &gamaFromFK)
{
	double goalRoll = sixdGoalPose.at(3);
	double goalPitch = sixdGoalPose.at(4);
	double goalYaw = sixdGoalPose.at(5);
	KDL::Rotation rotGoal = KDL::Rotation::EulerZYX(goalRoll,goalPitch,goalYaw);
	KDL::Rotation rotEF = FKPose.M.EulerZYX(alphaFromFK,betaFromFK,gamaFromFK);
	double cosTheta5 = rotEF(0,0) * rotGoal(0,0) + rotEF(1,0) * rotGoal(1,0) + rotEF(2,0) * rotGoal(2,0);
	//std::cout<<"cosTheta5 "<< cosTheta5<<std::endl;
	double sinTheta5 = rotEF(0,1) * rotGoal(0,0) + rotEF(1,1) * rotGoal(1,0) + rotEF(2,1) * rotGoal(2,0);
	//std::cout<<"sinTheta5 "<< sinTheta5<<std::endl;
	double Theta5 = atan2(sinTheta5,cosTheta5);
	//std::cout<<"The value for thetaFive is "<<Theta5<<std::endl;

	this->IKSolutions(0,5) = Theta5;
	this->IKSolutions(1,5) = Theta5;
	this->IKSolutions(2,5) = Theta5;
	this->IKSolutions(3,5) = Theta5;
}

bool YouBotAnalyticalSolver::ComputeIK(KDL::Frame cartpos)
{
	double Beta,Theta2Positive,Theta2Negative,Theta3Positive,Theta3Negative,alphaFromFK,betaFromFK,gamaFromFK;
	Beta=Theta2Positive=Theta2Negative=Theta3Positive=Theta3Negative=alphaFromFK= betaFromFK= gamaFromFK= 0;
	convertFKPoseToEuler(cartpos,alphaFromFK,betaFromFK,gamaFromFK);
	setUserInputAsFKinput(alphaFromFK,betaFromFK,gamaFromFK);
	calculateBetaAndTheta(Beta);
	calculateThetaThree(Beta,Theta3Positive,Theta3Negative);
	calculateThetaTwo(Beta,Theta2Positive,Theta2Negative,Theta3Positive,Theta3Negative);
	calculateThetaFour(Beta,Theta2Positive, Theta2Negative,Theta3Positive,Theta3Negative);
	// obj->compute();
	calculateBasePositionsXandY();
	calculateThetaFive(cartpos,alphaFromFK,betaFromFK,gamaFromFK);
	return true;
}
