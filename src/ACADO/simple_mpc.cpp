#include <acado_toolkit.hpp>
#include <include/acado_gnuplot/gnuplot_window.hpp>
#include <math.h>
int main( ) 
{  
    USING_NAMESPACE_ACADO
    // INTRODUCE THE VARIABLES:
    // -------------------------
    DifferentialState xB;
    DifferentialState xW;
    DifferentialState vB;
    
    Disturbance R;
    Control u;
    Control v;
    Control omega;
    Parameter mB;
    double mW = 50.0;
    double kS = 20000.0;
    double kT = 200000.0;
    // DEFINE THE DYNAMIC SYSTEM:
    // --------------------------
    DifferentialEquation f;
    f << dot(xB) == sin(vB)*u;
    f << dot(xW) == cos(vB)*v;
    f << dot(vB) == omega;
   
    OutputFcn g;
    g << xB;
    g << xW;
    g << vB;
    DynamicSystem dynSys( f,g );
    // SETUP THE PROCESS:
    // ------------------
    Process myProcess;
    myProcess.setDynamicSystem( dynSys,INT_RK45 );
    myProcess.set( ABSOLUTE_TOLERANCE,1.0e-8 );
    Vector x0( 3 );
    x0.setZero( );
    x0( 0 ) = 0.01;
    myProcess.initializeStartValues( x0 );
    myProcess.setProcessDisturbance( "road.txt" );
    myProcess.set( PLOT_RESOLUTION,HIGH );
    GnuplotWindow window;
        window.addSubplot( xB, "Robot Position x" );
        window.addSubplot( xW, "Robot Position y" );
        window.addSubplot( vB, "Angular Position z" );
        window.addSubplot( u,"x velocity" );
        window.addSubplot( v,"y velocity" );
        window.addSubplot( omega,"Angular Velocity" );
        window.addSubplot( R, "Road Disturbance" );
        window.addSubplot( g(0),"Output 1" );
        window.addSubplot( g(1),"Output 2" );
       window.addSubplot( g(2),"Output 2" );
    myProcess << window;
    // SIMULATE AND GET THE RESULTS:
    // -----------------------------
    VariablesGrid u( 1,0.0,1.0,7 );
    u( 0,0 ) = 10.0;
    u( 1,0 ) = -200.0;
    u( 2,0 ) = 200.0;
    u( 3,0 ) = 0.0;
    u( 4,0 ) = 0.0;
    u( 5,0 ) = 0.0;
    Vector p( 1 );
    p(0) = 350.0;
    myProcess.init( 0.0 );
    myProcess.run( u,p );
    VariablesGrid xSim, ySim;
    myProcess.getLast( LOG_SIMULATED_DIFFERENTIAL_STATES,xSim );
    xSim.print( "Simulated Differential States" );
    myProcess.getLast( LOG_PROCESS_OUTPUT,ySim );
    ySim.print( "Process Output" );
    return 0;
}
