/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <iostream>
#include <vector>
#include <signal.h>
#include <fstream>
#include "youbot/YouBotBase.hpp"
#include "youbot/YouBotManipulator.hpp"
#include "youbot/DataTrace.hpp"

using namespace std;
using namespace youbot;

bool running = true;

void sigintHandler(int signal) {
  running = false;

  std::cout << std::endl << " Interrupt!" << std::endl;

}

int main() {


ofstream j5;
j5.open("j5.txt");
ofstream j4;
j4.open("j4.txt");
ofstream j3;
j3.open("j3.txt");
ofstream j2;
j2.open("j2.txt");
ofstream j1;
j1.open("j1.txt");
//signal(SIGINT, sigintHandler);

try {

//rhama
//EthercatMasterInterface* ethercatMaster = &EthercatMaster::getInstance("youbot-ethercat.cfg", "../config/", false);
YouBotManipulator myArm("youbot-manipulator");
myArm.doJointCommutation();
myArm.calibrateManipulator();

DataTrace myTrace1(myArm.getArmJoint(1), "PositionTestArm6ResultJoint1");
DataTrace myTrace2(myArm.getArmJoint(2), "PositionTestArm6ResultJoint2");
DataTrace myTrace3(myArm.getArmJoint(3), "PositionTestArm6ResultJoint3");
DataTrace myTrace4(myArm.getArmJoint(4), "PositionTestArm6ResultJoint4");
DataTrace myTrace5(myArm.getArmJoint(5), "PositionTestArm6ResultJoint5");



std::vector<JointAngleSetpoint> jointSetAngle;
jointSetAngle.resize(5);

jointSetAngle[0].angle =  2.96244 * radian;
jointSetAngle[1].angle = 1.04883 * radian;
jointSetAngle[2].angle =-2.43523* radian;
jointSetAngle[3].angle = 1.73184  * radian;
jointSetAngle[4].angle =  2.91062 * radian;
cout << "going to candle position \n";

myArm.setJointData(jointSetAngle);
//myArm.getArmJoint(1).setData(setAngle);
SLEEP_SEC(5);

myArm.setJointData(jointSetAngle);
SLEEP_SEC(5);



int startTimeStep1 = 1500; // initial pose
int overallTime= 10500;



myTrace1.startTrace();
myTrace2.startTrace();
myTrace3.startTrace();
myTrace4.startTrace();
myTrace5.startTrace();

while (myTrace1.getTimeDurationMilliSec() < overallTime) {

	if (myTrace1.getTimeDurationMilliSec() > startTimeStep1 ){
		jointSetAngle[0].angle = (2.96244 - 1.57) * radian;
		jointSetAngle[1].angle = (1.04883 + 1.569) * radian;
		jointSetAngle[2].angle = (-2.43523 - 1.57) * radian;
		jointSetAngle[3].angle = (1.73184 + 1.57)  * radian;
		jointSetAngle[4].angle =  (2.91062 - 1.57) * radian;
	}


		myArm.setJointData(jointSetAngle);
		myTrace1.updateTrace(jointSetAngle[0].angle);
		myTrace2.updateTrace(jointSetAngle[1].angle);
		myTrace3.updateTrace(jointSetAngle[2].angle);
		myTrace4.updateTrace(jointSetAngle[3].angle);
		myTrace5.updateTrace(jointSetAngle[4].angle);

		SLEEP_MICROSEC(2500);
	}


//EthercatMaster::destroy();
JointSensedCurrent current;
JointSensedTorque torque;
while(true)
{

	myArm.getArmJoint(5).getData(torque);
	j5 <<torque.torque.value()<<endl;
       	myArm.getArmJoint(4).getData(torque);
	j4 <<torque.torque.value()<<endl;
	myArm.getArmJoint(3).getData(torque);
	j3 <<torque.torque.value()<<endl;
	myArm.getArmJoint(2).getData(torque);
	j2 <<torque.torque.value()<<endl;
	myArm.getArmJoint(1).getData(torque);
	j1 <<torque.torque.value()<<endl;
       
        
        /*if (torque.torque.value() > -0.5 || torque.torque.value() < -1.5){
		cout << "External Force Detected" << endl;
		//break;
	}
        else{
		std::cout << "Current manipulator joint 4: " << torque.torque.value()<< std::endl;
	}*/
	std::cout << "Current manipulator joint 5: " << torque.torque.value()<< std::endl;
	jointSetAngle[4].angle =  0.11062 * radian;
	jointSetAngle[0].angle = 0.0100693 * radian;
	jointSetAngle[1].angle = 0.0100693 * radian;
	jointSetAngle[2].angle = -0.015708 * radian;
	jointSetAngle[3].angle = 0.0221239 * radian;
	jointSetAngle[4].angle =  0.11062 * radian;
	SLEEP_MICROSEC(2500);

//try to read and print the joint 4 current for external forces ...Praveen

}

myTrace1.stopTrace();
myTrace2.stopTrace();
myTrace3.stopTrace();
myTrace4.stopTrace();
myTrace5.stopTrace();

myTrace1.plotTrace();
myTrace2.plotTrace();	
myTrace3.plotTrace();
myTrace4.plotTrace();
myTrace5.plotTrace();
SLEEP_SEC(5);



jointSetAngle[0].angle = 0.0100693 * radian;
jointSetAngle[1].angle = 0.0100693 * radian;
jointSetAngle[2].angle = -0.015708 * radian;
jointSetAngle[3].angle = 0.0221239 * radian;
jointSetAngle[4].angle =  0.11062 * radian;
myArm.setJointData(jointSetAngle);
cout << "going to Home position as an external force was detected \n";	
SLEEP_SEC(5);

} catch (std::exception& e) {
	std::cout <<"Exception: "<< e.what() << std::endl;
} catch (...) {
	std::cout << "unhandled exception" << std::endl;
}

return 0;
}
