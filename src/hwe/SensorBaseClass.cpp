/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#include <youbot_parser/SensorBaseClass.h>


namespace HardwareEncapsulation {

double SensorBaseClass::BaseAcceleration = 0.0;

SensorBaseClass::SensorBaseClass() {

	this->youbot_hardware = RobotHardware::getInstance();
	JointTorques = KDL::JntArray(youbot_hardware->nrofJoints);
	JointAngles = KDL::JntArray(youbot_hardware->nrofJoints);
	JointCurrents = KDL::JntArray(youbot_hardware->nrofJoints);
	BaseVelocity = KDL::JntArrayVel(3);
	JointAcceleration= KDL::JntArrayAcc(youbot_hardware->nrofJoints);
	JointVelocities = KDL::JntArrayVel(youbot_hardware->nrofJoints);
	this->BaseAcceleration = 0.0;
}

void SensorBaseClass::SensorUpdate(){

	getJointTorques();
	getJointAngles();
	getJointCurrents();
	getBaseVelocity();
	getJointAcceleration();
	getJointVelocities();
	getBaseAcceleration();

}

void SensorBaseClass::getJointAngles(){

	youbot::JointSensedAngle angle;
	for(unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		youbot_hardware->youBotArm->getArmJoint(i+1).getData(angle);
		this->JointAngles(i) = (double)angle.angle.value();
	}

}

void SensorBaseClass::getJointTorques(){

	youbot::JointSensedTorque tau;
	for(unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		youbot_hardware->youBotArm->getArmJoint(i+1).getData(tau);
		this->JointTorques(i) = (double)tau.torque.value();
	}

}

void SensorBaseClass::getJointCurrents(){

	youbot::JointSensedCurrent current;
	for(unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		youbot_hardware->youBotArm->getArmJoint(i+1).getData(current);
		this->JointCurrents(i) = (double)current.current.value();
	}


}
void SensorBaseClass::getBaseVelocity(){


}
void SensorBaseClass::getJointAcceleration(){

	//TODO: To be implemented

}
void SensorBaseClass::getJointVelocities(){

	/*youbot::JointSensedVelocity vel;
	for(unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		youbot_hardware->youBotArm->getArmJoint(i+1).getData(vel);
		this->JointVelocities(i) = (double)vel.angularVelocity.value();
	}*/

}
void SensorBaseClass::getBaseAcceleration(){

}


SensorBaseClass::~SensorBaseClass() {
	// TODO Auto-generated destructor stub
}

} /* namespace HardwareEncapsulation */
