/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <youbot_parser/RobotHardware.h>


namespace HardwareEncapsulation {

bool RobotHardware::instanceFlag =false; //for Singleton
RobotHardware* RobotHardware::single=NULL; //for Singleton

RobotHardware::RobotHardware() {
	// the whole setup is initialized with in the constructor
	youBotBase = new youbot::YouBotBase("youbot-base");
	this->youBotBase->doJointCommutation();
	std::cout<<"RESETING ENCODER TO ZERO"<<std::endl;
	for (unsigned int i=1;i<=4;i++)
		youBotBase->getBaseJoint(i).setEncoderToZero();
	this->youBotArm = new youbot::YouBotManipulator("youbot-manipulator");
	this->youBotArm->doJointCommutation();
	this->youBotArm->calibrateManipulator();
	/*if (startcalib()){
		std::cout << "Personalized Calibration Performed" << std::endl;

	}*/
	this->nrofJoints=5;


}

bool RobotHardware::startcalib(){
	std::vector<youbot::JointAngleSetpoint> jointSetAngle;
	jointSetAngle.resize(5);
	jointSetAngle[0].angle =  2.96244 * radian;
	jointSetAngle[1].angle = 1.04883 * radian;
	jointSetAngle[2].angle =-2.43523* radian;
	jointSetAngle[3].angle = 1.73184  * radian;
	jointSetAngle[4].angle =  2.91062 * radian;
	youBotArm->setJointData(jointSetAngle);
	//myArm.getArmJoint(1).setData(setAngle);
	SLEEP_SEC(5);
	for (unsigned int i=1;i<=5;i++)
		youBotArm->getArmJoint(i).setEncoderToZero();
	return true;
}

RobotHardware::~RobotHardware() {
	// Static flag set to false. usually to be called in the end of life cycle
	instanceFlag = false;

}
RobotHardware* RobotHardware::getInstance(){

	if(! instanceFlag)
	{
		single = new RobotHardware();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
}

} /* namespace HardwareEncapsulation */
