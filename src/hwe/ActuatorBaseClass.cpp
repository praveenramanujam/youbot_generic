/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <youbot_parser/ActuatorBaseClass.h>

namespace HardwareEncapsulation {

ActuatorBaseClass::ActuatorBaseClass() {
	youbot_hardware = RobotHardware::getInstance();

}

bool ActuatorBaseClass::checkJointLimits(KDL::JntArray jointpositions){

	return true;
}

bool ActuatorBaseClass::ActuateBase(const KDL::JntArray &values,const std::string &str){

	if(str == "current"){
		std::vector<youbot::JointCurrentSetpoint> currentSetpoint;
		currentSetpoint.resize(4);
		for (unsigned int i=0;i<4;i++)
			currentSetpoint[i].current = values(i)*ampere;
		youbot_hardware->youBotBase->setJointData(currentSetpoint);
		SLEEP_MICROSEC(2000);

	}

	else if (str == "torque")
	{
		std::vector<youbot::JointTorqueSetpoint> setVel;
		youbot::JointSensedTorque torque;
		setVel.resize(4);
		for (unsigned int i=0;i<4;i++)
			setVel[i].torque = 0.0*newton_meters;
		while(true){
			youbot_hardware->youBotBase->setJointData(setVel);
			youbot_hardware->youBotBase->getBaseJoint(4).getData(torque);
			std::cout<<"Torque:: " <<torque.torque.value()<<std::endl;
			SLEEP_MICROSEC(2000);
		}
	}

	return true;
}

bool ActuatorBaseClass::ActuateArm(const KDL::JntArray &jointpositions){

	std::vector<youbot::JointAngleSetpoint> jointSetAngle;
	jointSetAngle.resize(youbot_hardware->nrofJoints);
	for (unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		jointSetAngle[i].angle = jointpositions(i)*radian;

	}
	youbot_hardware->youBotArm->setJointData(jointSetAngle);
	SLEEP_SEC(5);
	youbot_hardware->youBotArm->setJointData(jointSetAngle);

	return true;
}
bool ActuatorBaseClass::ActuateArmRealtime(const KDL::JntArray &jointpositions){

	std::vector<youbot::JointAngleSetpoint> jointSetAngle;
	jointSetAngle.resize(youbot_hardware->nrofJoints);
	for (unsigned int i=0;i<youbot_hardware->nrofJoints;i++){
		jointSetAngle[i].angle = jointpositions(i)*radian;

	}
	youbot_hardware->youBotArm->setJointData(jointSetAngle);
	youbot_hardware->youBotArm->setJointData(jointSetAngle);

	return true;
}
ActuatorBaseClass::~ActuatorBaseClass() {
	// TODO Auto-generated destructor stub
}

} /* namespace HardwareEncapsulation */
