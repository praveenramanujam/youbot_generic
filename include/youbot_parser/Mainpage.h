/** \file Mainpage.h 
 * \mainpage  - Robot Modelling and Control
 * \image html youbot.png
 *
 * This is the documentation of Robot Modelling and Control. The
 * package aims for generic robot modelling and control for robot. Main Features include:
 *
 * \li System Identification and Parameter estimation of a robot.
 * \li Observers such as estimated external force.
 * \li Hybrid Postion/Force Control.
 *
 * Furthermore it contains functions for forward and inverse kinematics and
 * contact handling.
 *
 * The code is written by <a
 * href="mailto:praveen.ramanujam@gmail.com">Praveen Ramanujam
 * <praveen.ramanujam@gmail.com></a> under the supervision of 
 * <a href="mailto:Paul.Ploeger@h-brs.de">Prof.Dr. Paul.G.Ploeger
 * <Paul.Ploeger@h-brs.de></a> and <a href="mailto:matthias.f.fueller@gmail.com"> 
 * Mr.Matthias.F.Fueller <matthias.f.fueller@gmail.com></a>. The experimental study is 
 * heavily inspired by the book "Rigid Body Dynamics Algorithms" of <a
 * href="http://royfeatherstone.org" target="_parent">Roy Featherstone</a>. The data structures 
 * 
 * \image html arch.jpg
 *
 * Development is taking place on <a
 * href="http://bitbucket.org" target="_parent">bitbucket.org</a>. The official repository
 * can be found at: .
 * 
 * The library comes with version 3 of the the
 * <a href="http://eigen.tuxfamily.org/" target="_parent">Eigen</a> math library. More
 * information about it can be found here:
 * <a href="http://eigen.tuxfamily.org/" target="_parent">http://eigen.tuxfamily.org/</a>
 *
 * \section Milestones :
 * <ul>
 * <li> 13. February 2013: Mass Matrix</li>
 * <li> 20. February 2013: External Force Estimation</li>
 * </ul>
 *
 * \section Example Example
 *
 * A simple example for creation of a model and computation of the forward
 * dynamics using the C++ API can be found \ref SimpleExample "here".
 *
 * Another example that uses the \ref luamodel_introduction Addon can be found \ref
 * LuaModelExample "here".
 * 
 * \section ModuleOverview API reference separated by functional modules
 * 
 * \li \ref model_group
 * \li \ref kinematics_group
 * \li \ref dynamics_group
 * \li \ref contacts_group
 *
 * \section Installation Installation
 *
 * Robot Modelling and Control Package depends on few external libraries to enhance faster development.
 * The below mentioned instructions described below will guide through the installation:
 * \verbatim
		1. YouBot Driver

		2. KDL
		
		3. RBDL

		4. OpenRAVE

		4. Robot Modelling and Control
\endverbatim
 */
