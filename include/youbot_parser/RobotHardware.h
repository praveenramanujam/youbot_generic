/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef ROBOTHARDWARE_H_
#define ROBOTHARDWARE_H_

#include "youbot/YouBotBase.hpp"
#include "youbot/YouBotManipulator.hpp"
#include <vector>

namespace HardwareEncapsulation {

/*The class Robot Hardware is a best candidate for
 * for Singelton Design Pattern as the hardware is initilazed once
 * and need to be created again in the life time of execution cycle
 * This class is specific to youbot
 * TODO : Make it generic to accept any robot setup.
 */
class RobotHardware {

private :
	RobotHardware();
    bool startcalib();
public:
	unsigned int nrofJoints;
	static bool instanceFlag;     // Instance Flag to check if the hardware was setup atleast once
	static RobotHardware *single;
	static RobotHardware* getInstance();  //Member Function exposed. Mainly used either by sensors or actuators
	youbot::YouBotManipulator *youBotArm;  //Used for testing only
	youbot::YouBotBase *youBotBase;       // Used for testing only
	//youbot::YouBotJoint *youbotJoint;     //To be used. Direct Acess to the joints is what we need.
	virtual ~RobotHardware();
};

} /* namespace HardwareEncapsulation */
#endif /* ROBOTHARDWARE_H_ */
