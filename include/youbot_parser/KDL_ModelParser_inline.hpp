/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef KDLMODELPARSERINLINE_H
#define KDLMODELPARSERINLINE_H

#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>

class KDLModelParser{

public:
	// Create the frame that will contain the results
	KDL::Frame cartpos;
	KDL::Chain chain;
	/*Default Constructor*/
	KDLModelParser() {
		KDL::Tree robot_kdltree;
		KDL::Chain chain;
		if (urdftokdl(robot_kdltree)){
			treetochain(robot_kdltree,chain);
		}
		this->chain = chain;
	};

	bool urdftokdl(KDL::Tree &robot_kdltree){
		std::cout << "Parsing URDF to form KDL tree..." << std::endl;
		if (!kdl_parser::treeFromFile("../robots/arm_singular.urdf", robot_kdltree)){
			ROS_ERROR("Failed to construct kdl tree");
			return false;
		}
		return true;
	}

	bool inverseKinematics(KDL::Chain chain){
		return true;
	}

        bool forwarddynamics(KDL::Chain chain){
		return true;
	}
        bool inversedynamics(KDL::Chain chain){
		return true;
	}

	bool forwardKinematics(){

		// Create solver based on kinematic chain
		KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);

		// Create joint array
		unsigned int nj = chain.getNrOfJoints();
		KDL::JntArray jointpositions = KDL::JntArray(nj);

		// Assign some values to the joint positions
		for(unsigned int i=0;i<nj;i++){
			float myinput;
			std::cout<< "Enter the position of joint " << i << ": ";
			std::cin>> myinput;
			/*printf ("Enter the position of joint %i: ",i);
			scanf ("%e",&myinput);*/
			jointpositions(i)=(double)myinput;
		}

		// Calculate forward position kinematics
		bool kinematics_status;
		kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
		if(kinematics_status>=0){
			std::cout << cartpos <<std::endl;
			return true;
		}else{
			return false;
		}

	}

	bool forwardKinematics(KDL::JntArray jntpositions,KDL::Frame &cartpos){

			// Create solver based on kinematic chain
			KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);
			// Calculate forward position kinematics
			bool kinematics_status;
			kinematics_status = fksolver.JntToCart(jntpositions,cartpos);
			this->cartpos = cartpos;
			if(kinematics_status>=0){
				std::cout <<"FK" << this->cartpos <<std::endl;
				return true;
			}else{
				return false;
			}

		}

	bool treetochain(KDL::Tree &robot_kdltree,KDL::Chain &chain){

		std::string chain_root="base_link";
		std::string chain_tip = "arm_link_5";
		if(!robot_kdltree.getChain(chain_root,chain_tip,chain)){
			return false;
		}
		return true;
	}

    void listallsegments(KDL::Tree &robot_kdltree){
    	KDL::SegmentMap segment_map;
    	segment_map = robot_kdltree.getSegments();

    		for (KDL::SegmentMap::const_iterator it = segment_map.begin(); it != segment_map.end(); ++it)
    		{
    			if (it->second.segment.getJoint().getType() != KDL::Joint::None)
    			{
    				std::string joint_name = it->second.segment.getJoint().getName();
    				std::string segment_name = it->first;
    				std::cout<<segment_name<<std::endl;
    			}
    		}
    }
	~KDLModelParser() {};
};
#endif


