/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#ifndef SENSORBASECLASS_H_
#define SENSORBASECLASS_H_

#include <kdl/jntarray.hpp>
#include <kdl/jntarrayacc.hpp>
#include <kdl/jntarrayvel.hpp>
#include "RobotHardware.h"

namespace HardwareEncapsulation {

/*
 * Base class for reading sensor values from the robot
 * To faciltate the reading of joints from the robot
 * hardware. Default Implementation is for youBot
 *
 */
class SensorBaseClass {

private:

	virtual void getJointTorques();
	virtual void getJointAngles();
	virtual void getJointCurrents();
	virtual void getBaseVelocity();
	virtual void getJointAcceleration();
	virtual void getJointVelocities();
	virtual void getBaseAcceleration();
	RobotHardware  *youbot_hardware;


public:

	KDL::JntArray JointTorques;
	KDL::JntArray JointAngles;
	KDL::JntArray JointCurrents;
	KDL::JntArrayVel BaseVelocity;
	KDL::JntArrayAcc JointAcceleration;
	KDL::JntArrayVel JointVelocities;
    static double BaseAcceleration;
    virtual void SensorUpdate();
	SensorBaseClass();
	virtual ~SensorBaseClass();
};

} /* namespace HardwareEncapsulation */
#endif /* SENSORBASECLASS_H_ */
