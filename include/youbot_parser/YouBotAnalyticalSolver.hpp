/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef YOUBOTANALYTICALSOLVER_H
#define YOUBOTANALYTICALSOLVER_H
#include "stdio.h"
#include <iostream>
#include <vector>
#include <string>
#include <kdl/frames.hpp>
#include <youbot_parser/KDL_ModelParser_inline.hpp>
#include <youbot_parser/YouBotAnalyticalSolver.hpp>

struct YbIKparams{
	double rhoOne, rhoTwo, rhoThree, verticalVariance;  // Redundancy parameters
};


class YouBotAnalyticalSolver
{
private:
	YbIKparams paramObj;
	std::vector<double> sixdGoalPose;
	double LengthSegment[4]; //Length of the segments
	KDL::Rotation FKRotInEuler;
	KDL::Vector FKVector;
	void convertFKPoseToEuler(KDL::Frame &FKPose, double &alphaFromFK,double &betaFromFK,double &gamaFromFK);
	void getGoalPoseFromUser();
	void setUserInputAsFKinput(double &alphaFromFK,double &betaFromFK,double &gamaFromFK);
	void calculateBetaAndTheta(double &Beta);
	void calculateThetaThree(const double &Beta,double &Theta3Positive,double &Theta3Negative);
	void calculateThetaTwo(const double &Beta,double &Theta2Positive,double &Theta2Negative,const double &Theta3Positive,const double &Theta3Negative);
	void calculateThetaFour(const double &Beta,const double &Theta2Positive, const double &Theta2Negative,const double &Theta3Positive,const double &Theta3Negative);
	void calculateBasePositionsXandY();
	void calculateThetaFive(KDL::Frame &FKPose, double &alphaFromFK,double &betaFromFK,double &gamaFromFK);
	//myclassobj *obj;

public:
	Eigen::MatrixXf IKSolutions;
	YouBotAnalyticalSolver()
	{
		IKSolutions.resize(4,8);
		sixdGoalPose.assign (6,0.0);
		paramObj.rhoTwo = .2;             //Length of the base 580 mm width 380 mm and height 140 mm
		paramObj.verticalVariance = .2;   //608mm - 147mm
		LengthSegment[0] = .075;
		LengthSegment[1] = .155;
		LengthSegment[2] = .135;
		LengthSegment[3] = .081;
		//obj = new myclassbj();
	};

	virtual ~YouBotAnalyticalSolver(){};
	bool ComputeIK(KDL::Frame cartpos);
};

#endif
