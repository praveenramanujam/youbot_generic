/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hocschule Bonn-Rhein-Sieg
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redstributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Hochschule Bonn-Rhein-Sieg nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <youbot_parser/pinv.hpp>
#include <Eigen/SVD>
#include <vector>
#include <signal.h>
#include <fstream>
#include "youbot/YouBotBase.hpp"
#include "youbot/YouBotManipulator.hpp"
#include "youbot/DataTrace.hpp"
#include <stdio.h>
#include <math.h>
#include <angles/angles.h>
#include <Eigen/Dense>
using namespace std;
using namespace youbot;
using namespace angles;

class KDLYoubotInterface{

private:
	youbot::JointSensedAngle angle;
	youbot::JointSensedTorque torque;
	KDL::Tree robot_kdltree;
	KDL::JntArray LowerLimits;
	KDL::JntArray UpperLimits;
	KDL::JntArray jointpositions;
	KDL::JntArray jointtorques;
	KDL::Chain chain;
	KDL::Frame cartpos;
	KDL::Jacobian jac;


	bool urdftokdl(KDL::Tree &robot_kdltree){

		std::cout << "Parsing URDF to form KDL tree..." << std::endl;
		if (!kdl_parser::treeFromFile("../robots/arm_singular.urdf", robot_kdltree)){
			ROS_ERROR("Failed to construct kdl tree");
			return false;
		}
		treetochain();
		return true;
	}


	bool treetochain(){

		std::string chain_root="arm_link_1";
		std::string chain_tip = "arm_link_5";
		if(!this->robot_kdltree.getChain(chain_root,chain_tip,this->chain)){
			return false;
		}
		return true;
	}

	void listallsegments(KDL::Tree &robot_kdltree){
		KDL::SegmentMap segment_map;
		segment_map = robot_kdltree.getSegments();

		for (KDL::SegmentMap::const_iterator it = segment_map.begin(); it != segment_map.end(); ++it)
		{
			if (it->second.segment.getJoint().getType() != KDL::Joint::None)
			{
				std::string joint_name = it->second.segment.getJoint().getName();
				std::string segment_name = it->first;
				std::cout<<segment_name<<std::endl;
			}
		}
	}

	bool checkforJointLimits(const KDL::JntArray &jointpositions){

		std::cout << "Checking for Joint Limits" << std::endl;
		unsigned int nj = chain.getNrOfJoints();
		unsigned int st = 0;
		for(unsigned int i=0;i<nj;i++){
			(bool) (jointpositions(i)<UpperLimits(i) && jointpositions(i) > LowerLimits(i))?(st=st+1):(st=st+0);
		}
		if (st < nj)
			return false;

		return true;
	}


public:

	youbot::YouBotManipulator *myArm;



	/*Default Constructor*/
	KDLYoubotInterface() {
		myArm = new youbot::YouBotManipulator("youbot-manipulator");
		myArm->doJointCommutation();
		myArm->calibrateManipulator();
		urdftokdl(this->robot_kdltree);
		jointpositions = KDL::JntArray(5);
		jointtorques = KDL::JntArray(5);
		this->jac.resize(this->chain.getNrOfJoints());
		LowerLimits = KDL::JntArray(5);
		UpperLimits = KDL::JntArray(5);
		LowerLimits(0) = (double)normalize_angle(from_degrees(-169));
		LowerLimits(1) = (double) normalize_angle(from_degrees(-65));
		LowerLimits(2) = (double) normalize_angle(from_degrees(-151));
		LowerLimits(3) = (double)normalize_angle(from_degrees(-102.5));
		LowerLimits(4) = (double)normalize_angle(from_degrees(-167.5));
		UpperLimits(0) = (double)normalize_angle(from_degrees(169));
		UpperLimits(1) = (double)normalize_angle(from_degrees(90));
		UpperLimits(2) = (double)normalize_angle(from_degrees(146));
		UpperLimits(3) = (double)normalize_angle(from_degrees(102.5));
		UpperLimits(4) = (double)normalize_angle(from_degrees(167.5));


	};

	bool UpdateJointAngles(){
		std::cout << "Updating Joint Angles" << std::endl;
		for(unsigned int i=0;i<5;i++){
			this->myArm->getArmJoint(i+1).getData(this->angle);
			this->jointpositions(i) = (double)this->angle.angle.value();

		}
		return true;
	}

	bool UpdateJointTorques(){

			for(unsigned int i=0;i<5;i++){
				this->myArm->getArmJoint(i+1).getData(this->torque);
				this->jointtorques(i) = (double)this->torque.torque.value();

			}
			std::cout << "Joint Torques Updated" << std::endl;
			return true;
		}

	bool ComputeEndEffectorPos(){


		UpdateJointAngles();
		// Create solver based on kinematic chain
		KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);

		// Create joint array
		unsigned int nj = chain.getNrOfJoints();
		KDL::JntArray jointpositions = KDL::JntArray(nj);

		// Assign some values to the joint positions
		for(unsigned int i=0;i<nj;i++){
			jointpositions(i)=this->jointpositions(i);
		}



		// Calculate forward position kinematics
		bool kinematics_status;
		kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
		if(kinematics_status>=0){
			//std::cout << cartpos<<std::endl;
			return true;
		}else{
			return false;
		}

	}

	KDL::Frame GetEndEffectorPos(){

		std::cout << "Requesting End Effector Position" << std::endl;
		ComputeEndEffectorPos();
		std::cout << "End Effector Positon Sent" << std::endl;
		return this->cartpos;

	}

	bool ComputeIK(const KDL::Frame& cartpos, KDL::JntArray &q_out){

		KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);
		KDL::ChainIkSolverVel_pinv  ikvelsolver = KDL::ChainIkSolverVel_pinv(chain);
		KDL::ChainIkSolverPos_NR ik_solver = KDL::ChainIkSolverPos_NR(chain,fksolver,ikvelsolver);

		unsigned int nj = chain.getNrOfJoints();
		KDL::JntArray q_init;
		q_init = KDL::JntArray(nj);
		q_out = KDL::JntArray(nj);
		UpdateJointAngles();
		// Assign some values to the joint positions
		for(unsigned int i=0;i<nj;i++){
			q_init(i)=this->jointpositions(i);
		}
		if (ik_solver.CartToJnt(q_init,cartpos,q_out) < 0)
			return false; //Something went wrong
		cout << "Solution Found" << endl;
		for(unsigned int i=0;i<4;i++){
			cout <<to_degrees(q_out(i))<< endl;
		}

		if (!checkforJointLimits(q_out))
		{
			return false; // Solution Found but not in Joint Limits
		}


		return true;

	}

	bool ComputeJacobian()
	{
		KDL::ChainJntToJacSolver jac_solver = KDL::ChainJntToJacSolver(chain);
		unsigned int nj = chain.getNrOfJoints();
		KDL::JntArray q_init = KDL::JntArray(nj);
		UpdateJointAngles();
		// Assign some values to the joint positions
		for(unsigned int i=0;i<nj;i++){
			q_init(i)=this->jointpositions(i);
		}
		std::cout << "The joint angles are"<<q_init.data << std::endl;
		bool jac_st = jac_solver.JntToJac(q_init,this->jac);
		cout << jac.data<<endl;
		return true;
	}
	bool ComputeMassMatrix()
	{
		/*yet to be implemented*/
		return true;
	}

	bool EstimateEndEffectorForce()
	{
		ComputeJacobian();
		UpdateJointTorques();

		Eigen::MatrixXf m = Eigen::MatrixXf::Random(4,6);
		m <<  jac.data(0,0),jac.data(1,0),jac.data(2,0),jac.data(3,0),jac.data(4,0),jac.data(5,0),
				jac.data(0,1),	jac.data(1,1),jac.data(2,1),jac.data(3,1),jac.data(4,1),jac.data(5,1),
				jac.data(0,2),jac.data(1,2),jac.data(2,2),jac.data(3,2),jac.data(4,2),jac.data(5,2),
				jac.data(0,3),jac.data(1,3),jac.data(2,3),jac.data(3,3),jac.data(4,3),jac.data(5,3);
		Eigen::JacobiSVD<Eigen::MatrixXf> svd(m, Eigen::ComputeThinU | Eigen::ComputeThinV);
		Eigen::VectorXf tau(4);
		tau[0] = - this->jointtorques(0);
		tau[1] = - this->jointtorques(1);
		tau[2] = - this->jointtorques(2);
		tau[3] = - this->jointtorques(3);
		cout << "Force is " << endl <<svd.solve(tau);




		return true;
	}

	/*
	 * ONLY TO SHOW DEMONSTRATION AND RUN TESTS DURING DEVELOPMENT > SHOULD NOT BE USED FOR APPLICATION
	 */

	void resolvedRateMotionControl()
	{
		ComputeJacobian();

	}

	~KDLYoubotInterface() {};
};


