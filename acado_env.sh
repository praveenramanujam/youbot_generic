#
# This file is part of ACADO Toolkit.
#
# ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
# Copyright (C) 2008-2011 by Boris Houska and Hans Joachim Ferreau.
# All rights reserved.
#
# ACADO Toolkit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# ACADO Toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with ACADO Toolkit; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

################################################################################
#
# Description:
#	ACADO Toolkit package configuration file
#
# Authors:
#	Milan Vukov, milan.vukov@esat.kuleuven.be
#
# Year:
#	2011 - 2013.
#
# NOTE:
#	- Linux/Unix only.
#
# Usage:
#	- Linux - Ubuntu:
#		* Users are supposed to source this file into ~/.bashrc file.
#
################################################################################

################################################################################
#
# Definitions for both users and developers.
#
################################################################################

# 
# Tell the user project where to find our headers, libraries and external
# packages, etc.
#
export ACADO_ENV_INCLUDE_DIRS="/home/pramanuj/ACADOtoolkit/include;/home/pramanuj/ACADOtoolkit/external_packages;/home/pramanuj/ACADOtoolkit/external_packages/csparse/;/home/pramanuj/ACADOtoolkit/external_packages/qpOASES-3.0beta/include;/home/pramanuj/ACADOtoolkit/build/include"
export ACADO_ENV_LIBRARY_DIRS="/home/pramanuj/ACADOtoolkit/build/libs;/home/pramanuj/ACADOtoolkit/build/external_packages/qpOASES-3.0beta/libs;/home/pramanuj/ACADOtoolkit/build/external_packages/csparse/libs;/home/pramanuj/ACADOtoolkit/build/external_packages/casadi/libs"
export ACADO_ENV_EXTERNAL_PACKAGES_DIR="/home/pramanuj/ACADOtoolkit/external_packages"
export ACADO_ENV_CMAKE_DIR="/home/pramanuj/ACADOtoolkit/cmake"
 
#
# List of ACADO static libraries
#
export ACADO_ENV_STATIC_LIBRARIES="acado_toolkit;acado_qpOASESextras;acado_csparse;acado_casadi"
#
# List of ACADO shared libraries
#
export ACADO_ENV_SHARED_LIBRARIES="acado_toolkit_s;acado_qpOASESextras_s;acado_csparse_s;acado_casadi_s"

#
# ACADO is shipped with embedded version of qpOASES. Here is specified
# where source and header files reside
#
export ACADO_ENV_QPOASES_EMBEDDED_SOURCES="/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/Bounds.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/CyclingManager.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/MessageHandling.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/QProblem.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/Utils.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/Constraints.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/Indexlist.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/QProblemB.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/SubjectTo.cpp;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC/EXTRAS/SolutionAnalysis.cpp"
export ACADO_ENV_QPOASES_EMBEDDED_INC_DIRS="/home/pramanuj/ACADOtoolkit/external_packages/qpoases/;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/INCLUDE;/home/pramanuj/ACADOtoolkit/external_packages/qpoases/SRC"

